<?php

namespace AppBundle\Repository;

/**
 * User_likeRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class User_likeRepository extends \Doctrine\ORM\EntityRepository
{
    public function getLikesInfo($newsId)
    {
        $sql = $this->createQueryBuilder('user_like')
            ->select('user_like')
            ->leftJoin('user_like.user','user')
            ->addSelect('user')
            ->where('user_like.news = :id')
            ->andwhere('user_like.isDeleted != 1')
            ->setParameter('id', $newsId)
            ->getQuery()
            ->getArrayResult();
        //dump($sql);
        //exit();
        return $sql;
    }

    public function getLike($likeId)
    {
        return $this->createQueryBuilder('user_like')
            ->select('user_like')
            ->where('user_like.like_id = :id')
            ->setParameter('id', $likeId)
            ->getQuery()
            ->getArrayResult();
    }


}
