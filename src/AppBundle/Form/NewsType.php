<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title',TextType::class,['attr' => ['class' => 'form-control']])
            ->add('body',TextareaType::class,['attr' => ['class' => 'form-control']])
            ->add('published_at', DateTimeType::class,[
                'attr' => ['class' => 'js-datepicker form-control'],
                'format'=>'yyyy-MM-dd',
                'widget' => 'single_text',
                'html5' => false,
            ])
            ->add('category', EntityType::class,[
                'class' => 'AppBundle:Category',
                'choice_label' => 'name',
                'attr' => ['class' => 'form-control']
            ])
            ->add('image', FileType::class, ['label' => 'Image','data_class' => null,'required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'app_bundle_news_type';
    }
}
