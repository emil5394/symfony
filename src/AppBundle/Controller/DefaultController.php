<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\News;
use AppBundle\Entity\User;
use AppBundle\Entity\User_like;
use AppBundle\Form\CategoryType;
use AppBundle\Form\NewsType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use NotificationsBundle\Command\ServerCommand;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\Session;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(SessionInterface $session,Request $request, LoggerInterface $logger)
    {
        $sessionId  = $session->getId();
        dump($sessionId);
        $intentions = 'secretWord';
        $csrfToken = $this->get('security.csrf.token_manager')->getToken($intentions);


        //$this->isCsrfTokenValid('test', $token);

        dump($csrfToken);

        $logger->info('We are logging!');
        // replace this example code with whatever you need
        //exit();
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'csrfToken'=>$csrfToken,
            'sessionId'=>$sessionId
        ]);
    }

    /**
     * @Route("/checkUserToken/" , name="checkUserToken")
     */
    public function checkUserTokenAction(SessionInterface $session ,Request $request)
    {
        return new Response('ok');
    }

    /**
     * @Route("/category/" , name="category")
     */
    public function categoryAction()
    {
        //$entityManager = $this->getDoctrine()->getManager();

        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        dump($categories);

        return $this->render('category/allCategories.html.twig', ['categories'=>$categories]);
    }

    /**
     * @Route("/category/add/" , name="category.add")
     */
    public function categoryAddAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class,$category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            dump($form->getData());
            // 3) Encode the password (you could also do this via Doctrine listener)
            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            dump($category);
            $entityManager->persist($category);
            $entityManager->flush();

            return $this->redirectToRoute('category');
        }
        return $this->render('category/categoryAdd.html.twig',[
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/category/edit/{id}" , name="category.edit")
     */
    public function categoryEditAction(Request $request,$id)
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findOneBy(['id'=>$id]);

        dump($category);

        $form = $this->createForm(CategoryType::class,$category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();
            return $this->redirectToRoute('category');
        }
        return $this->render('category/editCategory.html.twig', [
            'category'=>$category,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/news/add/" , name="news.add")
     */
    public function newsAddAction(UserInterface $user,Request $request)
    {
        $news = new News();
        $news->setPublishedAt(new \DateTime());
        $form = $this->createForm(NewsType::class,$news);
        $userId = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['id'=>$user->getId()]);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        { //'data' => new \DateTime()
            /** @var UploadedFile $file */
            $file = $news->getImage();
            $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('news_directory'),
                $fileName
            );
            $news->setImage($fileName);
            $news->setUser($userId);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($news);
            $entityManager->flush();
            return $this->redirectToRoute('news');
        }
        return $this->render('news/newsAdd.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/news/edit/{id}" , name="news.edit")
     */
    public function newsEditAction(Request $request, EntityManagerInterface $entityManager,$id)
    {
        //$this->denyAccessUnlessGranted('edit',null);
        $news = $entityManager->getRepository(News::class)
            ->findOneBy(['id'=>$id]);

        try
        {
            $this->denyAccessUnlessGranted('edit', $news);
        }
        catch (\Exception $e)
        {
            return $this->render('exceptions/403error.twig');
        }


        $oldImage =  $news->getImage();

        $form = $this->createForm(NewsType::class,$news);
        $form->handleRequest($request);



        if($form->isSubmitted() && $form->isValid())
        {
            /** @var UploadedFile $file */
            $file = $news->getImage();
            if($file instanceof UploadedFile)
            {
                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('news_directory'),
                    $fileName
                );
                $news->setImage($fileName);
            }
            else
            {
                $news->setImage($oldImage);
            }
            dump($news);
            $entityManager->persist($news);
            $entityManager->flush();
            return $this->redirectToRoute('news');
        }
        return $this->render('news/newsAdd.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/news/delete/{id}",name="news.delete")
     */

    public function deleteNewsAction($id, EntityManagerInterface $entityManager)
    {
        $news = $entityManager->getRepository(News::class)->findOneBy(['id'=>$id]);
        $entityManager->remove($news);
        $entityManager->flush();
        return $this->redirectToRoute('news');
    }


    /**
     * @Route("/news/",name="news")
     */

    public function newsAction(Request $request, EntityManagerInterface $em)
    {
        $dataSet = $em->getRepository('AppBundle:News')->findAllOrderedById();

        $dql   = "SELECT a FROM AppBundle:News a";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1),
            $request->query->getInt('limit',3)
        );
        $pagination->setParam('publishedAt', new \DateTime());
        dump($pagination);
        //dump($dataSet); die;
        return $this->render('news/news.html.twig', ['data' => $pagination]);
    }

    /**
     * @Route("/news/show/{id}" ,options = { "expose" = true }, name="news.show",requirements={"id"="\d+"})
     */
    public function newsShowAction(UserInterface $user,$id)
    {
        $user_id = $user->getId();

        $news = $this->getDoctrine()
            ->getRepository(News::class)
            ->findOneBy(['id'=>$id]);

        $likes = $this->getDoctrine()
            ->getRepository(User_like::class)
            ->getLikesInfo($id);

        $ableToEdit = $user_id == $news->getUser()->getId() ? true : false;

        $likes_count = count($likes);

        $is_liked = 0;

        foreach ($likes as $key => $like)
        {
            if($like['user']['id']==$user_id)
            {
                $is_liked = 1;
            }
        }

        return $this->render('category/newsShow.html.twig',['news'=>$news,
            'likes_count'=>$likes_count,
            'is_liked'=>$is_liked,
            'ableToEdit'=> $ableToEdit,
            'user_id'=>$user_id]);
    }

    /**
     * @Route("/news/show/likePost",
     *     options = { "expose" = true },
     *     name="news.show.likePost")
     */

    public function likePostAction(Request $request,ValidatorInterface $validator)
    {
        $user_id = $request->request->get('user_id');
        $post_id = $request->request->get('id');

        $user_id_errors = $validator->validate($user_id, [new NotBlank(), new Type('numeric')]);
        $post_id_errors = $validator->validate($post_id, [new NotBlank(), new Type('numeric')]);

        if (count($user_id_errors) > 0)
        {
            $errorsString = (string) $user_id_errors;
            return new Response($errorsString);
        }
        if (count($post_id_errors) > 0)
        {
            $errorsString = (string) $post_id_errors;
            return new Response($errorsString);
        }


        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($user_id);
        $post = $em->getRepository('AppBundle:News')->find($post_id);

        $like = $em->getRepository('AppBundle:User_like')->findOneBy(['user'=>$user,'news'=>$post,'isDeleted'=>0]);


        if (!$like)
        {
            $like = new User_like();
            $like->setUser($user);
            $like->setNews($post);
            $em->persist($like);
            $em->flush();
        }
        else
        {
            $like->setIsDeleted('1');
            $em->persist($like);
            $em->flush();
        }
        return new Response('ok');
    }
    /**
     * @Route("/news/fetchNews",name="news.fetchNews")
     */

    public function fetchNewsAction(Request $request, EntityManagerInterface $em,ValidatorInterface $validator)
    {
        $date = $request->get('date');
        /*$date_errors = $validator->validate($date, [new Type('date')]);

        if (count($date_errors) > 0)
        {
            $errorsString = (string) $date_errors;
            return new Response($errorsString);
        }*/

        //$paginator  = $this->get('knp_paginator');
        //$pagination = $paginator->paginate(
        //    $dataSet, /* query NOT result */
        //    $request->query->getInt('page', 1)/*page number*/,
        //    2/*limit per page*/
        //);
        //dump($pagination);

        if($date=='')
        {
            $dataSet = $em->getRepository('AppBundle:News')->findAllOrderedById();
        }
        else
        {
            $dataSet = $em->getRepository('AppBundle:News')->findAllInDate(new \DateTime($date));
        }
        $result = json_encode(['status'=>'ok','dataSet'=>json_encode($dataSet, JSON_PRETTY_PRINT)]);
        return new Response($result);
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
