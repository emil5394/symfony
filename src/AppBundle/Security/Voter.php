<?php
/**
 * Created by PhpStorm.
 * User: b.emil
 * Date: 14.05.2018
 * Time: 11:26
 */

namespace AppBundle\Security;


abstract class Voter implements VoterInterface
{
    abstract protected function supports($attribute, $subject);
    abstract protected function voteOnAttribute($attribute, $subject, TokenInterface $token);
}