<?php
namespace NotificationsBundle\WebSocket;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Symfony\Component\HttpFoundation\Session\Session;



class WsEvents implements MessageComponentInterface
{
    protected $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn)
    {
        // Store the new connection to send messages to later
        parse_str($conn->httpRequest->getUri()->getQuery(), $queryParameters);
        $sessionId =  $queryParameters['sessionId'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://localhost:8000/checkUserToken/");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Cookie: PHPSESSID=$sessionId"
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            http_build_query(array('sessionId' => $sessionId)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);

        echo $server_output;

        //$intentions = 'secretWord';
        //$csrfToken = $this->get('security.csrf.token_manager')->getToken($intentions);

        if($server_output=='ok')
        {
            $this->clients->attach($conn);
            echo "New connection! ({$conn->resourceId})\n";
        }
        else
        {
            echo 'invalid connection';
        }
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        foreach ($this->clients as $client)
        {
            if ($from !== $client)
            {
                // The sender is not the receiver, send to each client connected
                $client->send($msg);
            }
        }


    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}