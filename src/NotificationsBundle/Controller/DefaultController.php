<?php

namespace NotificationsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/Notifications")
     */
    public function indexAction()
    {
        return $this->render('@Notifications/Default/index.html.twig');
    }

}
