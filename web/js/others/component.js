// Namespaces
let Component = {};
Component.Plugin = {};

Component.Plugin.PluginManager = {};

/**
 * Initializes plugins placed in context
 *
 * @param {Object} context jQuery object
 */
Component.Plugin.PluginManager.init = function (context) {

    context = this._getContext(context);

    context.find('[data-plugin]').each(function () {
        let element = $(this),
            pluginId = element.data('plugin'),
            pluginParams = element.data('plugin-params') || {};

        Component.Plugin.Plugin.init(element, pluginId, pluginParams);
    });

};

Component.Plugin.PluginManager._getContext = function (context) {

    if (_.isUndefined(context)) {
        // default context
        context = $('body');
    }

    if (!context instanceof $) {
        throw new TypeError("First argument of PluginManager must be instance of jQuery");
    }

    return context;

};

Component.Plugin.Plugin = {};

Component.Plugin.getById = function (id) {
    let map = {
        "timepicker"	: "timepickerPlugin",
        "select2"		: "select2Plugin",
        "select2-ajax"	: "select2AjaxPlugin",
        "selectize"		: "selectizePlugin",
        "dropzone"		: "dropzonePlugin",
        "datepicker"	: "datepickerPlugin",
        "datepickerJUI"	: "datepickerJUIPlugin",
        "cropit"		: "cropitPlugin",
        "inputmask"		: "inputmaskPlugin",
        "date"			: "datepickerPlugin",
        "datetime"		: "datetimepickerPlugin",
        "time"			: "timepickerPlugin",
        "color"			: "colorpickerPlugin",
        "uniform"		: "uniformPlugin",
        "spinner"		: "spinnerPlugin"
    };

    if (map.hasOwnProperty(id)) {
        return this.Plugin[map[id]];
    } else {
        console.error(`Plugin with id '${id}' is not registered`);
        return null;
    }
};

Component.Plugin.Plugin.init = function (element, pluginId, pluginParams) {

    let plugin = Component.Plugin.getById(pluginId)

    plugin.init(element, pluginParams);
    // setting value
    if (!_.isUndefined(pluginParams.value)) {
        if (plugin.setValue) {
            // plugin has custom value setter
            plugin.setValue(element, pluginParams.value);
        }
        else {
            element.val(pluginParams.value);
        }
    }

};

/**
 * Sets default parameters and return object with merged parameters
 *
 * @param params Actual params
 * @param defaultParams
 * @returns {Object}
 * @private
 */
Component.Plugin.Plugin._getParams = function (params, defaultParams) {
    params = params || {};
    return _.defaults(params, defaultParams);
};

Component.Plugin.Plugin.timepickerPlugin = {
    init: function (element, params) {
        params = Component.Plugin.Plugin._getParams(params, {
            minuteStep: 1,
            explicitMode: true,
            defaultTime: false,
            showMeridian: false
        });

        element.timepicker(params);
    }
};

Component.Plugin.Plugin.colorpickerPlugin = {
    init: function (element, params) {
        params = Component.Plugin.Plugin._getParams(params, {
            format: 'hex'
        });

        element.colorpicker(params);
    }
};

Component.Plugin.Plugin.select2Plugin = {
    init: function (element, params) {
        params = Component.Plugin.Plugin._getParams(params, {
            allowClear: true
        });

        element.select2(params);
    },
    setValue: function (element, value) {
        element.select2('val', value);
    }
};

Component.Plugin.Plugin.select2AjaxPlugin = {
    settings: {
        url: "includes/plugins/axtarish.php"
    },
    init: function (element, params) {
        params = Component.Plugin.Plugin._getParams(params, {
            allowClear: true
        });

        let options = {
            allowClear: params.allowClear,
            multiple: params.multiple || false,
            ajax: {
                url: this.settings.url,
                type: 'POST',
                dataType: 'json',
                data: function (soz)
                {
                    if (_.isFunction(params.getAjaxData)) {
                        var data = params.getAjaxData(element);
                    } else {
                        var data = Object.create(null);
                        if (!_.isUndefined(params.queryString)) {
                            Object.assign(data, params.queryString);
                        }
                    }

                    data.a = soz;
                    return data;
                },
                results: function(data,a)
                {
                    return data
                },
                cache: true
            }
        };

        element.select2(options);

        if (params.data) {
            element.select2('data', params.data);
        }
    },
    setValue: function (element, value) {
        if (_.isNull(value)) {
            return;
        }

        if (_.isUndefined(value.text)) {
            var pluginParams = element.data('plugin-params') || {};

            if (_.isUndefined(pluginParams.queryString)) {
                return;
            }

            pluginParams.queryString.id = value;
            pluginParams.queryString.a = '';

            $.post(this.settings.url, pluginParams.queryString, function(response) {
                if (response.results.length > 0) {
                    element.select2('data', response.results[0]);
                }
            }, 'json');

            return;
        }

        if (_.isNull(value.text) || _.isNull(value.id)) {
            return;
        }

        element.select2('data', value);
    }
};

Component.Plugin.Plugin.selectizePlugin = {
    init: function (element, params) {
        params = Component.Plugin.Plugin._getParams(params, {});
        element.selectize(params);
    },
    setValue: function (element, value) {
        element.selectize()[0].selectize.setValue(value)
    }
};

Component.Plugin.Plugin.datepickerPlugin = {
    init: function (element, params) {
        params = Component.Plugin.Plugin._getParams(params, {
            format: "dd-mm-yyyy",
            language: "az",
            todayHighlight: true,
            autoclose: true
        });
        element.datepicker(params);
    }
};

Component.Plugin.Plugin.datepickerJUIPlugin = {
    init: function (element, params) {
        params = Component.Plugin.Plugin._getParams(params, {
            dateFormat: "dd-mm-yy",
            regional: "az",
            todayHighlight: true,
            autoclose: true
        });
        element.datepicker(params);
    }
};

Component.Plugin.Plugin.datetimepickerPlugin = {
    init: function (element, params) {
        params = Component.Plugin.Plugin._getParams(params, {
            format: "dd-mm-yyyy hh:ii",
            language: "az",
            todayHighlight: true,
            autoclose: true
        });
        element.datetimepicker(params);
    }
};

Component.Plugin.Plugin.inputmaskPlugin = {
    init: function (element, params) {
        element.inputmask( "mask", params );
    }
};

Component.Plugin.Plugin.uniformPlugin = {
    init: function (element, params) {
        element.uniform();
    },
    setValue: function (element, value) {
        element
            .prop('checked', !!+value)
            .trigger('change')
            .uniform('refresh');
    }
};

Component.Plugin.Plugin.spinnerPlugin = {
    init: function (element, params) {
        element.spinner();
    }
};

Component.Plugin.Plugin.dropzonePlugin = {
    init: function (element, params) {
        params = Component.Plugin.Plugin._getParams(params, {
            url: "null",
            autoProcessQueue: false,
            addRemoveLinks: true,
            dictRemoveFile: "Sil"
        });

        element.dropzone(params);
    }
};

Component.Plugin.Plugin.cropitPlugin = {
    init: function (element, params) {
        params = Component.Plugin.Plugin._getParams(params, {
            width: 235,
            height: 235,
            imageState: {
                src: '',
            }
        });

        element.cropit(params);

        element.find('.rotate-cw').click(function () {
            element.cropit('rotateCW');
        });
        element.find('.rotate-ccw').click(function () {
            element.cropit('rotateCCW');
        });
        element.find('.image-select').click(function () {
            $(this).parents('div.image-editor').find('.cropit-image-input').click();
        });
    }
};

/**
 * Loads content remotely or locally to target element
 *
 * @param {string}   params.src|params.url HTML string or URL
 * @param {boolean}  params.remote
 * @param {boolean}  params.append if true, data is appended to targetElement
 * @param {Object}   params.targetElement jQuery
 * @param {callback} params.onLoad
 */
Component.loadContent = function (params) {

    // targetElement, url, parameters
    let parameters = params.parameters,
        source = params.url || params.src,
        targetElement = params.targetElement,
        onLoad = params.onLoad,
        append = params.append;

    // load html to targetElement from string source
    if (params.remote === false) {
        append ? targetElement.append(source) : targetElement.html(source);
        Component.Plugin.PluginManager.init(targetElement);
    }
    else {
        sehifeLoading(1);
        $.post(source, parameters, (response, status, XHR) => {
            let content,
            responseType = XHR.getResponseHeader("Content-type");

        if (responseType === "application/json") {
            content = response.html || response.form;
        }
        else {
            content = response;
        }

        append ? targetElement.append(content) : targetElement.html(content);
        Component.Plugin.PluginManager.init(targetElement);

        if (_.isFunction(onLoad)) {
            onLoad(response, status, XHR);
        }

        sehifeLoading(0);
    });
    }


};

Component.Form = {};
Component.Form.send = function (params) {

    let onSuccess = params.success;
    let ajaxParams = _.omit(params, ['form', 'success']);
    let formData;

    params = _.defaults(params, {
        useDefaultFormData: true
    });

    if (!(params.form instanceof FormData)) {
        if (params.form.tagName === "FORM" && true === params.useDefaultFormData) {
            formData = new FormData(params.form);
        } else {
            formData = new FormData();
            $(params.form).find('[name]').each((index, element) => {
                let name = $(element).attr('name');
            let value = $(element).val();
            let elementType = $(element).attr('type');

            if (elementType === "checkbox") {
                if (params.sendUncheckedCheckbox) {
                    $(element).is(":checked") ?
                        formData.append(name, params.checkboxCheckedValue) :
                        formData.append(name, params.checkboxUncheckedValue);
                } else {
                    if ($(element).is(":checked")) {
                        formData.append(name, "on")
                    }
                }
            } else if (elementType === "file") {
                Array.prototype.forEach.call($(element).get(0).files, function (file) {
                    formData.append(name, file);
                });
            } else {
                formData.append(name, value)
            }
        });
        }
    }
    else {
        formData = params.form;
    }

    if (_.isObject(params.overwrite)) {
        _.mapObject(params.overwrite, function (val, key) {
            formData.set(key, val);
        });
    }

    // getting files from dropzone plugin
    let dropzonePlugin = $(params.form).find('[data-plugin=dropzone]');
    if (dropzonePlugin.length) {
        let fileFieldName = dropzonePlugin.attr('name');
        Dropzone.forElement(dropzonePlugin.get(0)).files.forEach((file) => {
            formData.append(fileFieldName, file);
        // formData.append(fileFieldName + "[]", file);
    });
    }

    let cropitPlugin = $(params.form).find('[data-plugin=cropit]');
    if (cropitPlugin.length) {
        let fileFieldName = cropitPlugin.attr('name');
        let imageData = cropitPlugin.cropit('export', {
            type: 'image/jpeg',
            quality: 0.33,
            originalSize: false
        });

        if (!_.isUndefined(imageData)) {
            // base64
            formData.append(fileFieldName, dataURItoBlob(imageData, 'image/jpeg'));
        }
    }

    let url = null;

    if (params.url) {
        url = params.url
    } else if (!(params.form instanceof FormData)) {
        url = params.form.getAttribute('action');
    }

    sehifeLoading(1);
    ajaxParams = _.defaults(ajaxParams, {
        url: url,
        type: 'POST',
        contentType: false,
        cache: false,
        processData: false,
        data: formData,
        success: function (data, textStatus, jqXHR) {
            if (_.isFunction(onSuccess)) {
                onSuccess(data, textStatus, jqXHR);
            }
            sehifeLoading(0);
        },
        complete: function () {
            sehifeLoading(0);
        }
    });

    $.ajax(ajaxParams);

};

Component.Form.showErrors = function (form, errors) {
    var errorsList = form.find(".errors_list");
    errorsList.html('');
    if (errors.length) {
        errors.forEach(function (error) {
            errorsList.append('<strong>' + error + '</strong><br>');
        });

        errorsList.slideDown();
        toastr.error('Səhv var!');
    } else {
        errorsList.slideUp();
    }

};

Component.Form.setData = function (form, data, params) {
    params = params || {};

    form.find('[name]').each(function(index, element) {
        var formElement     = $(this);
        var formElementName = formElement.attr('name');
        var pluginId        = formElement.attr('data-plugin');
        var hasPlugin       = !_.isUndefined(pluginId);

        if (_.contains(params.ignore, formElementName)) {
            return;
        }

        if (_.isUndefined(data[formElementName])) {
            return;
        }

        var value = data[formElementName];

        if (hasPlugin) {
            var plugin = Component.Plugin.getById(pluginId);
            if (plugin.setValue) {
                // plugin has custom value setter
                plugin.setValue(formElement, value);
            }
            else {
                formElement.val(value);
            }
        } else {
            formElement.val(value);
        }
    });
};

Component.Table = {};

/**
 * Summarizes values of cells of the specific column
 *
 * @param thElement jQuery
 */
Component.Table.getColumnTotal = function (thElement) {
    let table = thElement.closest('table'),
        columnOrder = thElement.index();

    let total = 0;
    table.find('tbody tr:visible').find('td:eq(' + columnOrder + ')').each((i, element) => {
        let currentNumber;

    currentNumber = parseFloat(
        this._getCellData($(element))
    );

    if (!isNaN(currentNumber)) {
        total += currentNumber;
    }
});

    return total;
};

/**
 * Returns number of rows
 *
 * @param table jQuery
 */
Component.Table.getRowNumber = function (table) {
    return table.find('tbody tr:visible').length;
};

/**
 * Returns table data as two-dimentional array
 *
 * @param rowsContainer table|tbody|thead jQuery
 * @return Array
 */
Component.Table.getArray = function (rowsContainer, params = {}) {
    // rows
    let tableData = [];

    // column names in the array below are not included to array
    let except = [];
    if (params.except) {
        except = this.convertColumnNamesToIndexes(params.except, rowsContainer);
    }

    let dataTransformerColumnIndexes = [];
    if (!_.isUndefined(params.dataTransformer)) {
        dataTransformerColumnIndexes = Object.keys(params.dataTransformer);

        // dataTransformerColumnIndexes = this.convertColumnNamesToIndexes(
        // 	Object.keys(params.dataTransformer), rowsContainer
        // );
    }

    rowsContainer.find('tr:visible:not([data-ignore="true"])').each((rowIndex, rowElement) => {
        let row = [];

    $(rowElement).find('td:visible, th:visible').each((cellIndex, cellElement) => {
        if (~except.indexOf(cellIndex)) {
        return;
    }

    let columnName = Component.Table.findColumnNameByIndex(
        cellIndex, rowsContainer
    );
    if (~dataTransformerColumnIndexes.indexOf(columnName)) {
        if (!_.isFunction(params.dataTransformer[columnName])) {
            throw new Error("Datatransformer must be function");
        }
        else {
            row.push(params.dataTransformer[columnName]($(cellElement)));
        }
    }
    else {
        row.push(this._getCellData($(cellElement), params));
    }

});

    tableData.push(row);
});

    return tableData;
};

/**
 * Returns data from td or th html element
 *
 * @param cell jQuery
 * @param params
 * @private
 */
Component.Table._getCellData = function (cell, params = {}) {
    let cellData;

    _.defaults(params, {
        checkedValue: true,
        uncheckedValue: false,
    });

    let input = cell.find('input, select');
    let hasTextData = $.trim(cell.text()).length > 0;
    // input?
    if (input.length) {
        if (input.attr('type') === "checkbox") {
            if (hasTextData) {
                cellData = cell.text();
            }
            else {
                cellData = input.is(':checked') ?
                    params.checkedValue :
                    params.uncheckedValue;
            }
        }
        else if (input.get(0).tagName === "SELECT") {
            cellData = input.text();
        }
        else {
            cellData = input.val();
        }
    }
    else {
        cellData = cell.text();
    }

    if (_.isString(cellData)) {
        return $.trim(cellData);
    }

    return cellData;
};

/**
 * Returns indexes of found rows
 *
 * @param {string} value
 * @param {Object} table jQuery
 */
Component.Table.search = function (value, table, params) {
    let indexes = [];

    let except = [];
    if (params.except) {
        except = this.convertColumnNamesToIndexes(params.except, table);
    }
    table.find('tbody tr').each(function (rowIndex, row) {
        $(row).find('td').each(function (cellIndex, cell) {
            let cellData = Component.Table._getCellData($(cell));
            if (!_.isString(cellData)) {
                return;
            }

            if (~cellData.toLowerCase().indexOf(value) && except.indexOf(cellIndex) === -1) {
                // found and td not in except array
                indexes.push(rowIndex);
                // break
                return false;
            }
        });
    });

    return indexes;
};

/**
 * Returns column index using data-name attribute of th
 *
 * @param columnName
 * @param table
 */
Component.Table.findColumnIndexByName = function (columnName, table, params = {}) {
    let foundIndex = null;
    let selector = 'th:visible';

    if (params.visible === false) {
        selector = 'th'
    }

    table.find(selector).each((index, th) => {
        if ($(th).data('name') === columnName) {
        foundIndex = index;
        return false;
    }
});

    return foundIndex;
};

Component.Table.findColumnNameByIndex = function (columnIndex, table) {
    return table.find('th:visible:eq(' + columnIndex + ')').data('name');
};

/**
 * Returns column indexes using data-name attribute of th
 *
 * @param columnNames array
 * @param table
 */
Component.Table.convertColumnNamesToIndexes = function (columnNames, table) {
    return columnNames.map((columnName) => {
            return this.findColumnIndexByName(columnName, table);
});
};

/**
 * Returns th element by its inner text
 *
 * @param search
 * @param table
 * @return thElement jQuery
 */
Component.Table.findColumnByInnerText = function (search, table) {
    let foundElement = null;
    table.find('th').each((index, th) => {
        if (~this._getCellData($(th)).indexOf(search)) {
        foundElement = $(th);
        return false;
    }
});

    return foundElement;
};

/**
 * Orders column
 *
 * @param thElement jQuery
 */
Component.Table.orderColumn = function (thElement) {
    let table = thElement.closest('table'),
        columnOrder = thElement.index();

    let order = 1;
    table.find('tbody tr:visible').find('td:eq(' + columnOrder + ')').each((i, element) => {
        $(element).text(order);
    order++;
});
};

Component.Event = {};

Component.Event.listeners = {};
Component.Event.on = function (eventName, callback) {
    if (!_.isFunction(callback)) {
        throw new TypeError("Argument 2 must be callable");
    }

    if (_.isUndefined(Component.Event.listeners[eventName])) {
        this.listeners[eventName] = [];
    }

    this.listeners[eventName].push(callback);
};

Component.Event.trigger = function (eventName, args) {
    if (_.isUndefined(this.listeners[eventName])) {
        return;
    }

    this.listeners[eventName].forEach(function (callback) {
        callback(args);
    });
};

Component.Math = {};

/**
 * Returns percent of totalNumber
 *
 * @param partialNumber
 * @param totalNumber
 */
Component.Math.getPercent = function (partialNumber, totalNumber) {
    return Math.round((partialNumber / totalNumber) * 100);
};

function dataURItoBlob(dataURI, type) {
    // convert base64 to raw binary data held in a string
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    var bb = new Blob([ab], {type: type});
    return bb;
}

// 9842
