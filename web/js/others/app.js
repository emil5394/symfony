var app = app || {};

app.pdfExport = (function () {

    /**
     * Exports table to PDF
     *
     * @param {string}   params.title
     * @param {Object}   params.table jQuery object
     * @param {number[]} params.sign  column names after which sign columns is appended
     * @param {Object[]} params.dataTransformer pair of column name and callback
     */
    function open(params) {

        var docDefinition = {};
        docDefinition.content = [];

        params =
            _.chain(params)
                .defaults({
                    title: "",
                    date: "",
                    username: ""
                })
                .mapObject(function (val) {
                    if (_.isString(val))
                        return $.trim(val);

                    return val;
                })
                .value();

        docDefinition.content.push(
            {
                columns: [
                    {
                        text: params.username + "\n",
                        style: 'header',
                        fontSize: 20
                    },
                    {
                        text: params.title,
                        style: 'header',
                        fontSize: 20,
                        alignment: 'right',
                    },
                ]
            },
            {
                text: params.date + "\n\n"
            }
        );

        var tableData = Component.Table.getArray(params.table, {
            // checkedTitle: "İşə gəlib",
            except: params.except,
            dataTransformer: params.dataTransformer
        });

        if (!tableData.length) {
            //alert('Cədvəl boşdu!');
            return;
        }

        // table with sign?
        if (_.isArray(params.sign)) {

            var thIndexes = Component.Table.convertColumnNamesToIndexes(
                params.sign,
                params.table
            );

            // new array with sign column
            var tableDataWithSign = [];

            tableData.forEach(function (row, rowIndex) {
                var rowWithSign = [];
                row.forEach(function (cell, index) {
                    rowWithSign.push(cell);

                    if (~thIndexes.indexOf(index)) {
                        rowWithSign.push(rowIndex === 0 ? "İmza": ""); // th or td?
                    }
                });

                if (_.contains(params.sign, '_last-column')) {
                    rowWithSign.push(rowIndex === 0 ? "İmza": ""); // th or td?
                }

                tableDataWithSign.push(rowWithSign);
            });


            tableData = tableDataWithSign;
        }

        var pdfmakeTableBody = _convertTableToPdfmakeFormat(tableData);
        var widths = Array(tableData[0].length).fill(String(100 / tableData[0].length) + '%');

        if (tableData[0][0] == "№") {
            // widths[0] = 20;
        }

        var hasFooter = params.table.find('tfoot#table-footer').length,
            lastRowNumber;
        if (hasFooter) {
            lastRowNumber = pdfmakeTableBody.length - 1;
            pdfmakeTableBody[lastRowNumber].forEach(function (element) {
                element.color = 'white';
                element.border = [false, false, false, false];
            });
        } else {
            lastRowNumber = -1;
        }

        docDefinition.content.push({
            fontSize: 9,
            style: 'tableExample',
            color: '#333',
            table: {
                widths: widths,
                body: pdfmakeTableBody,
            },
            layout: {
                hLineColor: function(i, row, node) {
                    return row === lastRowNumber ? 'black' : 'gray';
                },
                vLineColor: function(i, row, node) {
                    return 'gray';
                },
                fillColor: function(row, node) { if (row === lastRowNumber) return 'black'; }
            }
        });

        pdfMake.createPdf(docDefinition).open();
    }

    function _convertTableToPdfmakeFormat(tableData) {
        var pdfmakeTable = [];

        tableData.forEach(function (row, rowIndex) {
            var newRow = [];
            row.forEach(function (cell) {
                if (rowIndex === 0) {
                    // table header
                    newRow.push({
                        text: cell,
                        style: 'tableHeader',
                        alignment: 'center',
                        bold: true
                    });
                } else {
                    newRow.push({
                        text: cell,
                        alignment: 'center'
                    });
                }

            });

            pdfmakeTable.push(newRow);
        });

        return pdfmakeTable;
    }

    return {
        open: open
    };

})();

/**
 * Shows confirmation dialog
 * @param {string}  params.title
 * @param {string}  params.text
 * @param {string}  params.state info|danger|success
 * @param {string}  params.type  alert|confirm|alert saved|confirm delete
 * @param {callback} params.onConfirm
 * @param {callback} params.onCancel
 */
app.confirm = (function () {

    function close(confirmBox) {
        confirmBox.find('div.notification').removeClass('jackInTheBox').addClass('zoomOut');
        setTimeout(function(){
            confirmBox.remove();
        }, 500);
    }

    return function (params) {
        if (params.type === 'alert saved') {
            params.title = 'Yadda saxlandı!';
            params.state = 'success';
            params.type  = 'alert';
        } else if (params.type === 'confirm delete') {
            params.title = 'Silmək istədiyinizə əminsinizmi?';
            params.state = 'danger';
            params.type  = 'confirm';
        }

        var confirmBoxTemplate = _.template($("#confirm-box").html());
        var confirmBox = $(confirmBoxTemplate({
            title: params.title,
            text:  params.text,
            state: params.state,
            type:  params.type || 'confirm'
        }));

        $('body').append(confirmBox);
        confirmBox.addClass('open');

        confirmBox.find('.notification_footer .btn[data-type]').on('click', function (e) {

            var type = $(this).data('type');
            var func = params['on' + _.capitalizeFirstChar(type)];

            if (_.isFunction(func)) {
                func.apply(this, arguments);
            }

            close(confirmBox);
        });

        confirmBox.find('.notifi_close').on('click', function () {
            close(confirmBox);
        });
    };

})();

/**
 * Formats PHP's datetime object
 * @param datetimeObject
 * @return {string}
 */
app.formatDate = function (datetimeObject, format = "DD-MM-YYYY" ) {
    if (!_.isObject(datetimeObject)) {
        return '-';
    }

    if (!_.isString(datetimeObject.date) || datetimeObject.date === '') {
        return '-';
    }

    return moment(datetimeObject.date, "YYYY-MM-DD").format(format);
};

/**
 * Formats PHP's datetime object
 * @param datetimeObject
 * @return {string}
 */
app.formatTime = function (datetimeObject, altString = '') {
    if (!_.isObject(datetimeObject)) {
        return altString;
    }

    if (!_.isString(datetimeObject.date) || datetimeObject.date === '') {
        return altString;
    }

    return moment(datetimeObject.date, "YYYY-MM-DD HH:mm:ss").format("HH:mm");
};

/**
 * @param {string}  templateId
 * @param {Object}  targetElement jQuery
 * @param {Array|Object} [templateData]
 * @param {string} [url] data source
 * @param {string|Array|Object} [data] The parameter can be an Url or a data itself
 * @param {boolean} [loop=false]
 * @param {boolean} [clearTarget=true] target is cleared before append
 * @param {boolean} [replace=false]
 * @param {string}  [altString = '']
 */
app.addFromTemplate = (function() {

    function getHTML(templateFunc, templateData, loop, altString) {
        if (loop) {
            var result = '';

            if (_.isEmpty(templateData)) {
                result = _.isUndefined(altString) ? '' : altString;
            } else {
                if (_.isArray(templateData)) {
                    templateData.forEach(function (templateData, index) {
                        templateData._index = index + 1;
                        result += templateFunc(templateData);
                    });
                } else {
                    for (var i in templateData) {
                        templateData[i]._key = i;
                        result += templateFunc(templateData[i]);
                    }
                }
            }

            return result;
        } else {
            templateData._index = templateData._index || "";
            templateData._key   = templateData._key || "";
            return templateFunc(templateData);
        }
    }

    return function(params) {
        _.checkRequiredProps(params, [
            'templateId',
            'targetElement'
        ]);

        params = _.defaults(params, {
            loop: false,
            clearTarget: true,
            replace: false
        });

        if (params.clearTarget && params.replace === false) {
            params.targetElement.html('');
        }

        var compiledTemplateFunc = _.template($("#" + params.templateId).html());

        if (params.data) {
            if (_.isString(params.data)) {
                params.url = params.data;
            } else {
                params.templateData = params.data;
            }
        }

        if (params.templateData) {
            var html = getHTML(compiledTemplateFunc, params.templateData, params.loop, params.altString);
            var element = $(html);
            params.replace ?
                params.targetElement.replaceWith(element) :
                params.targetElement.append(element);

            Component.Plugin.PluginManager.init(element);

            return element;
        } else {
            Component.loadContent({
                url: params.url,
                targetElement: params.targetElement,
                onLoad: params.onLoad,
                append: true,
                processResponseData: function (templateData) {
                    return getHTML(compiledTemplateFunc, templateData, params.loop, params.altString);
                }
            });
        }
    }

})();

app.tenantRouting = {};
app.tenantRouting.generate = function (routeName, params) {
    params = params || {};
    params.company = tenant.slug;
    return Routing.generate(routeName, params);
};