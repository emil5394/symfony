<?php return array (
  'lifetime' => 0,
  'data' => 
  Gos\Bundle\PubSubRouterBundle\Router\RouteCollection::__set_state(array(
     'routes' => 
    array (
      'acme_a_b_c_rpc' => 
      Gos\Bundle\PubSubRouterBundle\Router\Route::__set_state(array(
         'pattern' => 'acme/{method}',
         'callback' => 'acme.method_abc.rpc',
         'args' => 
        array (
        ),
         'requirements' => 
        array (
          'method' => 
          array (
            'path' => 'method_a|method_b|method_c',
          ),
        ),
         'name' => NULL,
      )),
      'acme_d_e_rpc' => 
      Gos\Bundle\PubSubRouterBundle\Router\Route::__set_state(array(
         'pattern' => 'sample/{method}',
         'callback' => 'acme.method_d_e.rpc',
         'args' => 
        array (
        ),
         'requirements' => 
        array (
          'method' => 
          array (
            'path' => 'method_d|method_e',
          ),
        ),
         'name' => NULL,
      )),
    ),
  )),
);