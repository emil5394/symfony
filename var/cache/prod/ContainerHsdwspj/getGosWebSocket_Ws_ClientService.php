<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'gos_web_socket.ws.client' shared service.

include_once $this->targetDirs[3].'\\vendor\\gos\\websocket-client\\Wamp\\Client.php';

$this->services['gos_web_socket.ws.client'] = $instance = new \Gos\Component\WebSocketClient\Wamp\Client('127.0.0.1', 8080);

if ($this->has('monolog.logger.websocket')) {
    $instance->setLogger(${($_ = isset($this->services['monolog.logger.websocket']) ? $this->services['monolog.logger.websocket'] : $this->load('getMonolog_Logger_WebsocketService.php')) && false ?: '_'});
}

return $instance;
