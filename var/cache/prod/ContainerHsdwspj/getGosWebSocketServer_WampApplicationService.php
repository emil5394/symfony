<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'gos_web_socket_server.wamp_application' shared service.

if ($lazyLoad) {
    return $this->services['gos_web_socket_server.wamp_application'] = $this->createProxy('WampApplication_812abca', function () {
        return new \WampApplication_812abca(function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
            $wrappedInstance = $this->load('getGosWebSocketServer_WampApplicationService.php', false);

            $proxy->setProxyInitializer(null);

            return true;
        });
    });
}

include_once $this->targetDirs[3].'\\vendor\\cboden\\ratchet\\src\\Ratchet\\ComponentInterface.php';
include_once $this->targetDirs[3].'\\vendor\\cboden\\ratchet\\src\\Ratchet\\Wamp\\WampServerInterface.php';
include_once $this->targetDirs[3].'\\vendor\\gos\\web-socket-bundle\\Server\\App\\WampApplication.php';

return new \Gos\Bundle\WebSocketBundle\Server\App\WampApplication(${($_ = isset($this->services['gos_web_socket.rpc.dispatcher']) ? $this->services['gos_web_socket.rpc.dispatcher'] : $this->load('getGosWebSocket_Rpc_DispatcherService.php')) && false ?: '_'}, ${($_ = isset($this->services['gos_web_socket.topic.dispatcher']) ? $this->services['gos_web_socket.topic.dispatcher'] : $this->load('getGosWebSocket_Topic_DispatcherService.php')) && false ?: '_'}, ${($_ = isset($this->services['event_dispatcher']) ? $this->services['event_dispatcher'] : $this->getEventDispatcherService()) && false ?: '_'}, ${($_ = isset($this->services['gos_web_socket.client_storage']) ? $this->services['gos_web_socket.client_storage'] : $this->load('getGosWebSocket_ClientStorageService.php')) && false ?: '_'}, ${($_ = isset($this->services['gos_web_socket.router.wamp']) ? $this->services['gos_web_socket.router.wamp'] : $this->load('getGosWebSocket_Router_WampService.php')) && false ?: '_'}, ${($_ = isset($this->services['monolog.logger.websocket']) ? $this->services['monolog.logger.websocket'] : $this->load('getMonolog_Logger_WebsocketService.php')) && false ?: '_'});
