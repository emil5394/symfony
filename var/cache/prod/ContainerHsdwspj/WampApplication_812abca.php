<?php

class WampApplication_812abca extends \Gos\Bundle\WebSocketBundle\Server\App\WampApplication implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder812abca = null;
    private $initializer812abca = null;
    private static $publicProperties812abca = array(
        
    );
    public function onPublish(\Ratchet\ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, 'onPublish', array('conn' => $conn, 'topic' => $topic, 'event' => $event, 'exclude' => $exclude, 'eligible' => $eligible), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        return $this->valueHolder812abca->onPublish($conn, $topic, $event, $exclude, $eligible);
    }
    public function onPush(\Gos\Bundle\WebSocketBundle\Router\WampRequest $request, $data, $provider)
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, 'onPush', array('request' => $request, 'data' => $data, 'provider' => $provider), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        return $this->valueHolder812abca->onPush($request, $data, $provider);
    }
    public function onCall(\Ratchet\ConnectionInterface $conn, $id, $topic, array $params)
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, 'onCall', array('conn' => $conn, 'id' => $id, 'topic' => $topic, 'params' => $params), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        return $this->valueHolder812abca->onCall($conn, $id, $topic, $params);
    }
    public function onSubscribe(\Ratchet\ConnectionInterface $conn, $topic)
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, 'onSubscribe', array('conn' => $conn, 'topic' => $topic), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        return $this->valueHolder812abca->onSubscribe($conn, $topic);
    }
    public function onUnSubscribe(\Ratchet\ConnectionInterface $conn, $topic)
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, 'onUnSubscribe', array('conn' => $conn, 'topic' => $topic), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        return $this->valueHolder812abca->onUnSubscribe($conn, $topic);
    }
    public function onOpen(\Ratchet\ConnectionInterface $conn)
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, 'onOpen', array('conn' => $conn), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        return $this->valueHolder812abca->onOpen($conn);
    }
    public function onClose(\Ratchet\ConnectionInterface $conn)
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, 'onClose', array('conn' => $conn), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        return $this->valueHolder812abca->onClose($conn);
    }
    public function onError(\Ratchet\ConnectionInterface $conn, \Exception $e)
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, 'onError', array('conn' => $conn, 'e' => $e), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        return $this->valueHolder812abca->onError($conn, $e);
    }
    public function __construct($initializer)
    {
        $this->initializer812abca = $initializer;
    }
    public function & __get($name)
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, '__get', array('name' => $name), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        if (isset(self::$publicProperties812abca[$name])) {
            return $this->valueHolder812abca->$name;
        }
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder812abca;
            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }
        $targetObject = $this->valueHolder812abca;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder812abca;
            return $targetObject->$name = $value;;
            return;
        }
        $targetObject = $this->valueHolder812abca;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, '__isset', array('name' => $name), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder812abca;
            return isset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolder812abca;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, '__unset', array('name' => $name), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder812abca;
            unset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolder812abca;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __clone()
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, '__clone', array(), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        $this->valueHolder812abca = clone $this->valueHolder812abca;
    }
    public function __sleep()
    {
        $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, '__sleep', array(), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
        return array('valueHolder812abca');
    }
    public function __wakeup()
    {
    }
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer812abca = $initializer;
    }
    public function getProxyInitializer()
    {
        return $this->initializer812abca;
    }
    public function initializeProxy()
    {
        return $this->initializer812abca && ($this->initializer812abca->__invoke($valueHolder812abca, $this, 'initializeProxy', array(), $this->initializer812abca) || 1) && $this->valueHolder812abca = $valueHolder812abca;
    }
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder812abca;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder812abca;
    }
}
