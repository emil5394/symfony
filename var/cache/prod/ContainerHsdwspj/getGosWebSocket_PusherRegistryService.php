<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'gos_web_socket.pusher_registry' shared service.

include_once $this->targetDirs[3].'\\vendor\\gos\\web-socket-bundle\\Pusher\\PusherRegistry.php';

return $this->services['gos_web_socket.pusher_registry'] = new \Gos\Bundle\WebSocketBundle\Pusher\PusherRegistry();
