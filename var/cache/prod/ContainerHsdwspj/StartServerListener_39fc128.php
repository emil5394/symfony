<?php

class StartServerListener_39fc128 extends \Gos\Bundle\WebSocketBundle\Event\StartServerListener implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder39fc128 = null;
    private $initializer39fc128 = null;
    private static $publicProperties39fc128 = array(
        
    );
    public function bindPnctlEvent(\Gos\Bundle\WebSocketBundle\Event\ServerEvent $event)
    {
        $this->initializer39fc128 && ($this->initializer39fc128->__invoke($valueHolder39fc128, $this, 'bindPnctlEvent', array('event' => $event), $this->initializer39fc128) || 1) && $this->valueHolder39fc128 = $valueHolder39fc128;
        return $this->valueHolder39fc128->bindPnctlEvent($event);
    }
    public function __construct($initializer)
    {
        $this->initializer39fc128 = $initializer;
    }
    public function & __get($name)
    {
        $this->initializer39fc128 && ($this->initializer39fc128->__invoke($valueHolder39fc128, $this, '__get', array('name' => $name), $this->initializer39fc128) || 1) && $this->valueHolder39fc128 = $valueHolder39fc128;
        if (isset(self::$publicProperties39fc128[$name])) {
            return $this->valueHolder39fc128->$name;
        }
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder39fc128;
            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }
        $targetObject = $this->valueHolder39fc128;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer39fc128 && ($this->initializer39fc128->__invoke($valueHolder39fc128, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer39fc128) || 1) && $this->valueHolder39fc128 = $valueHolder39fc128;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder39fc128;
            return $targetObject->$name = $value;;
            return;
        }
        $targetObject = $this->valueHolder39fc128;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer39fc128 && ($this->initializer39fc128->__invoke($valueHolder39fc128, $this, '__isset', array('name' => $name), $this->initializer39fc128) || 1) && $this->valueHolder39fc128 = $valueHolder39fc128;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder39fc128;
            return isset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolder39fc128;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer39fc128 && ($this->initializer39fc128->__invoke($valueHolder39fc128, $this, '__unset', array('name' => $name), $this->initializer39fc128) || 1) && $this->valueHolder39fc128 = $valueHolder39fc128;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder39fc128;
            unset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolder39fc128;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __clone()
    {
        $this->initializer39fc128 && ($this->initializer39fc128->__invoke($valueHolder39fc128, $this, '__clone', array(), $this->initializer39fc128) || 1) && $this->valueHolder39fc128 = $valueHolder39fc128;
        $this->valueHolder39fc128 = clone $this->valueHolder39fc128;
    }
    public function __sleep()
    {
        $this->initializer39fc128 && ($this->initializer39fc128->__invoke($valueHolder39fc128, $this, '__sleep', array(), $this->initializer39fc128) || 1) && $this->valueHolder39fc128 = $valueHolder39fc128;
        return array('valueHolder39fc128');
    }
    public function __wakeup()
    {
    }
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer39fc128 = $initializer;
    }
    public function getProxyInitializer()
    {
        return $this->initializer39fc128;
    }
    public function initializeProxy()
    {
        return $this->initializer39fc128 && ($this->initializer39fc128->__invoke($valueHolder39fc128, $this, 'initializeProxy', array(), $this->initializer39fc128) || 1) && $this->valueHolder39fc128 = $valueHolder39fc128;
    }
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder39fc128;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder39fc128;
    }
}
