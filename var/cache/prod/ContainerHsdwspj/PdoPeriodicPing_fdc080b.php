<?php

class PdoPeriodicPing_fdc080b extends \Gos\Bundle\WebSocketBundle\Periodic\PdoPeriodicPing implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolderfdc080b = null;
    private $initializerfdc080b = null;
    private static $publicPropertiesfdc080b = array(
        
    );
    public function setTimeout($timeout)
    {
        $this->initializerfdc080b && ($this->initializerfdc080b->__invoke($valueHolderfdc080b, $this, 'setTimeout', array('timeout' => $timeout), $this->initializerfdc080b) || 1) && $this->valueHolderfdc080b = $valueHolderfdc080b;
        return $this->valueHolderfdc080b->setTimeout($timeout);
    }
    public function tick()
    {
        $this->initializerfdc080b && ($this->initializerfdc080b->__invoke($valueHolderfdc080b, $this, 'tick', array(), $this->initializerfdc080b) || 1) && $this->valueHolderfdc080b = $valueHolderfdc080b;
        return $this->valueHolderfdc080b->tick();
    }
    public function getTimeout()
    {
        $this->initializerfdc080b && ($this->initializerfdc080b->__invoke($valueHolderfdc080b, $this, 'getTimeout', array(), $this->initializerfdc080b) || 1) && $this->valueHolderfdc080b = $valueHolderfdc080b;
        return $this->valueHolderfdc080b->getTimeout();
    }
    public function __construct($initializer)
    {
        $this->initializerfdc080b = $initializer;
    }
    public function & __get($name)
    {
        $this->initializerfdc080b && ($this->initializerfdc080b->__invoke($valueHolderfdc080b, $this, '__get', array('name' => $name), $this->initializerfdc080b) || 1) && $this->valueHolderfdc080b = $valueHolderfdc080b;
        if (isset(self::$publicPropertiesfdc080b[$name])) {
            return $this->valueHolderfdc080b->$name;
        }
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderfdc080b;
            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }
        $targetObject = $this->valueHolderfdc080b;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializerfdc080b && ($this->initializerfdc080b->__invoke($valueHolderfdc080b, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerfdc080b) || 1) && $this->valueHolderfdc080b = $valueHolderfdc080b;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderfdc080b;
            return $targetObject->$name = $value;;
            return;
        }
        $targetObject = $this->valueHolderfdc080b;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializerfdc080b && ($this->initializerfdc080b->__invoke($valueHolderfdc080b, $this, '__isset', array('name' => $name), $this->initializerfdc080b) || 1) && $this->valueHolderfdc080b = $valueHolderfdc080b;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderfdc080b;
            return isset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolderfdc080b;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializerfdc080b && ($this->initializerfdc080b->__invoke($valueHolderfdc080b, $this, '__unset', array('name' => $name), $this->initializerfdc080b) || 1) && $this->valueHolderfdc080b = $valueHolderfdc080b;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderfdc080b;
            unset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolderfdc080b;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __clone()
    {
        $this->initializerfdc080b && ($this->initializerfdc080b->__invoke($valueHolderfdc080b, $this, '__clone', array(), $this->initializerfdc080b) || 1) && $this->valueHolderfdc080b = $valueHolderfdc080b;
        $this->valueHolderfdc080b = clone $this->valueHolderfdc080b;
    }
    public function __sleep()
    {
        $this->initializerfdc080b && ($this->initializerfdc080b->__invoke($valueHolderfdc080b, $this, '__sleep', array(), $this->initializerfdc080b) || 1) && $this->valueHolderfdc080b = $valueHolderfdc080b;
        return array('valueHolderfdc080b');
    }
    public function __wakeup()
    {
    }
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializerfdc080b = $initializer;
    }
    public function getProxyInitializer()
    {
        return $this->initializerfdc080b;
    }
    public function initializeProxy()
    {
        return $this->initializerfdc080b && ($this->initializerfdc080b->__invoke($valueHolderfdc080b, $this, 'initializeProxy', array(), $this->initializerfdc080b) || 1) && $this->valueHolderfdc080b = $valueHolderfdc080b;
    }
    public function isProxyInitialized()
    {
        return null !== $this->valueHolderfdc080b;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderfdc080b;
    }
}
