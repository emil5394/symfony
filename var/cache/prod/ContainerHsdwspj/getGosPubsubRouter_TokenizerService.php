<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'gos_pubsub_router.tokenizer' shared service.

include_once $this->targetDirs[3].'\\vendor\\gos\\pubsub-router-bundle\\Tokenizer\\TokenizerInterface.php';
include_once $this->targetDirs[3].'\\vendor\\gos\\pubsub-router-bundle\\Tokenizer\\Tokenizer.php';
include_once $this->targetDirs[3].'\\vendor\\gos\\pubsub-router-bundle\\Tokenizer\\TokenizerCacheDecorator.php';
include_once $this->targetDirs[3].'\\vendor\\doctrine\\cache\\lib\\Doctrine\\Common\\Cache\\Cache.php';
include_once $this->targetDirs[3].'\\vendor\\gos\\pubsub-router-bundle\\Cache\\PhpFileCacheDecorator.php';

return $this->services['gos_pubsub_router.tokenizer'] = new \Gos\Bundle\PubSubRouterBundle\Tokenizer\TokenizerCacheDecorator(new \Gos\Bundle\PubSubRouterBundle\Tokenizer\Tokenizer(), ${($_ = isset($this->services['gos_pubsub_router.php_file.cache']) ? $this->services['gos_pubsub_router.php_file.cache'] : $this->services['gos_pubsub_router.php_file.cache'] = new \Gos\Bundle\PubSubRouterBundle\Cache\PhpFileCacheDecorator($this->targetDirs[0], false)) && false ?: '_'});
