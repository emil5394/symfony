<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'gos_pubsub_router.generator' shared service.

include_once $this->targetDirs[3].'\\vendor\\gos\\pubsub-router-bundle\\Generator\\GeneratorInterface.php';
include_once $this->targetDirs[3].'\\vendor\\gos\\pubsub-router-bundle\\Generator\\Generator.php';
include_once $this->targetDirs[3].'\\vendor\\gos\\pubsub-router-bundle\\Router\\RouteCollection.php';

$this->services['gos_pubsub_router.generator'] = $instance = new \Gos\Bundle\PubSubRouterBundle\Generator\Generator(${($_ = isset($this->services['gos_pubsub_router.tokenizer']) ? $this->services['gos_pubsub_router.tokenizer'] : $this->load('getGosPubsubRouter_TokenizerService.php')) && false ?: '_'});

$instance->setCollection(${($_ = isset($this->services['gos_pubsub_router.collection.websocket']) ? $this->services['gos_pubsub_router.collection.websocket'] : $this->services['gos_pubsub_router.collection.websocket'] = new \Gos\Bundle\PubSubRouterBundle\Router\RouteCollection()) && false ?: '_'});

return $instance;
