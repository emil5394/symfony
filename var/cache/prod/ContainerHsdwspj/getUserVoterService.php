<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'user_voter' shared autowired service.

include_once $this->targetDirs[3].'\\vendor\\symfony\\symfony\\src\\Symfony\\Component\\Security\\Core\\Authorization\\Voter\\VoterInterface.php';
include_once $this->targetDirs[3].'\\vendor\\symfony\\symfony\\src\\Symfony\\Component\\Security\\Core\\Authorization\\Voter\\Voter.php';
include_once $this->targetDirs[3].'\\src\\AppBundle\\Security\\NewsVoter.php';

return $this->services['user_voter'] = new \AppBundle\Security\NewsVoter();
