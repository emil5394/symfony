<?php

class WampRouter_8bdcb43 extends \Gos\Bundle\WebSocketBundle\Router\WampRouter implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder8bdcb43 = null;
    private $initializer8bdcb43 = null;
    private static $publicProperties8bdcb43 = array(
        
    );
    public function setContext(\Gos\Bundle\PubSubRouterBundle\Router\RouterContext $context)
    {
        $this->initializer8bdcb43 && ($this->initializer8bdcb43->__invoke($valueHolder8bdcb43, $this, 'setContext', array('context' => $context), $this->initializer8bdcb43) || 1) && $this->valueHolder8bdcb43 = $valueHolder8bdcb43;
        return $this->valueHolder8bdcb43->setContext($context);
    }
    public function getContext()
    {
        $this->initializer8bdcb43 && ($this->initializer8bdcb43->__invoke($valueHolder8bdcb43, $this, 'getContext', array(), $this->initializer8bdcb43) || 1) && $this->valueHolder8bdcb43 = $valueHolder8bdcb43;
        return $this->valueHolder8bdcb43->getContext();
    }
    public function match(\Ratchet\Wamp\Topic $topic, $tokenSeparator = null)
    {
        $this->initializer8bdcb43 && ($this->initializer8bdcb43->__invoke($valueHolder8bdcb43, $this, 'match', array('topic' => $topic, 'tokenSeparator' => $tokenSeparator), $this->initializer8bdcb43) || 1) && $this->valueHolder8bdcb43 = $valueHolder8bdcb43;
        return $this->valueHolder8bdcb43->match($topic, $tokenSeparator);
    }
    public function generate($routeName, array $parameters = array(), $tokenSeparator = null)
    {
        $this->initializer8bdcb43 && ($this->initializer8bdcb43->__invoke($valueHolder8bdcb43, $this, 'generate', array('routeName' => $routeName, 'parameters' => $parameters, 'tokenSeparator' => $tokenSeparator), $this->initializer8bdcb43) || 1) && $this->valueHolder8bdcb43 = $valueHolder8bdcb43;
        return $this->valueHolder8bdcb43->generate($routeName, $parameters, $tokenSeparator);
    }
    public function getCollection()
    {
        $this->initializer8bdcb43 && ($this->initializer8bdcb43->__invoke($valueHolder8bdcb43, $this, 'getCollection', array(), $this->initializer8bdcb43) || 1) && $this->valueHolder8bdcb43 = $valueHolder8bdcb43;
        return $this->valueHolder8bdcb43->getCollection();
    }
    public function __construct($initializer)
    {
        $this->initializer8bdcb43 = $initializer;
    }
    public function & __get($name)
    {
        $this->initializer8bdcb43 && ($this->initializer8bdcb43->__invoke($valueHolder8bdcb43, $this, '__get', array('name' => $name), $this->initializer8bdcb43) || 1) && $this->valueHolder8bdcb43 = $valueHolder8bdcb43;
        if (isset(self::$publicProperties8bdcb43[$name])) {
            return $this->valueHolder8bdcb43->$name;
        }
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8bdcb43;
            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }
        $targetObject = $this->valueHolder8bdcb43;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer8bdcb43 && ($this->initializer8bdcb43->__invoke($valueHolder8bdcb43, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer8bdcb43) || 1) && $this->valueHolder8bdcb43 = $valueHolder8bdcb43;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8bdcb43;
            return $targetObject->$name = $value;;
            return;
        }
        $targetObject = $this->valueHolder8bdcb43;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer8bdcb43 && ($this->initializer8bdcb43->__invoke($valueHolder8bdcb43, $this, '__isset', array('name' => $name), $this->initializer8bdcb43) || 1) && $this->valueHolder8bdcb43 = $valueHolder8bdcb43;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8bdcb43;
            return isset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolder8bdcb43;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer8bdcb43 && ($this->initializer8bdcb43->__invoke($valueHolder8bdcb43, $this, '__unset', array('name' => $name), $this->initializer8bdcb43) || 1) && $this->valueHolder8bdcb43 = $valueHolder8bdcb43;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8bdcb43;
            unset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolder8bdcb43;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __clone()
    {
        $this->initializer8bdcb43 && ($this->initializer8bdcb43->__invoke($valueHolder8bdcb43, $this, '__clone', array(), $this->initializer8bdcb43) || 1) && $this->valueHolder8bdcb43 = $valueHolder8bdcb43;
        $this->valueHolder8bdcb43 = clone $this->valueHolder8bdcb43;
    }
    public function __sleep()
    {
        $this->initializer8bdcb43 && ($this->initializer8bdcb43->__invoke($valueHolder8bdcb43, $this, '__sleep', array(), $this->initializer8bdcb43) || 1) && $this->valueHolder8bdcb43 = $valueHolder8bdcb43;
        return array('valueHolder8bdcb43');
    }
    public function __wakeup()
    {
    }
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer8bdcb43 = $initializer;
    }
    public function getProxyInitializer()
    {
        return $this->initializer8bdcb43;
    }
    public function initializeProxy()
    {
        return $this->initializer8bdcb43 && ($this->initializer8bdcb43->__invoke($valueHolder8bdcb43, $this, 'initializeProxy', array(), $this->initializer8bdcb43) || 1) && $this->valueHolder8bdcb43 = $valueHolder8bdcb43;
    }
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder8bdcb43;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder8bdcb43;
    }
}
