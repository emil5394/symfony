<?php

class TopicDispatcher_2fb069a extends \Gos\Bundle\WebSocketBundle\Server\App\Dispatcher\TopicDispatcher implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder2fb069a = null;
    private $initializer2fb069a = null;
    private static $publicProperties2fb069a = array(
        
    );
    public function onSubscribe(\Ratchet\ConnectionInterface $conn, \Ratchet\Wamp\Topic $topic, \Gos\Bundle\WebSocketBundle\Router\WampRequest $request)
    {
        $this->initializer2fb069a && ($this->initializer2fb069a->__invoke($valueHolder2fb069a, $this, 'onSubscribe', array('conn' => $conn, 'topic' => $topic, 'request' => $request), $this->initializer2fb069a) || 1) && $this->valueHolder2fb069a = $valueHolder2fb069a;
        return $this->valueHolder2fb069a->onSubscribe($conn, $topic, $request);
    }
    public function onPush(\Gos\Bundle\WebSocketBundle\Router\WampRequest $request, $data, $provider)
    {
        $this->initializer2fb069a && ($this->initializer2fb069a->__invoke($valueHolder2fb069a, $this, 'onPush', array('request' => $request, 'data' => $data, 'provider' => $provider), $this->initializer2fb069a) || 1) && $this->valueHolder2fb069a = $valueHolder2fb069a;
        return $this->valueHolder2fb069a->onPush($request, $data, $provider);
    }
    public function onUnSubscribe(\Ratchet\ConnectionInterface $conn, \Ratchet\Wamp\Topic $topic, \Gos\Bundle\WebSocketBundle\Router\WampRequest $request)
    {
        $this->initializer2fb069a && ($this->initializer2fb069a->__invoke($valueHolder2fb069a, $this, 'onUnSubscribe', array('conn' => $conn, 'topic' => $topic, 'request' => $request), $this->initializer2fb069a) || 1) && $this->valueHolder2fb069a = $valueHolder2fb069a;
        return $this->valueHolder2fb069a->onUnSubscribe($conn, $topic, $request);
    }
    public function onPublish(\Ratchet\ConnectionInterface $conn, \Ratchet\Wamp\Topic $topic, \Gos\Bundle\WebSocketBundle\Router\WampRequest $request, $event, array $exclude, array $eligible)
    {
        $this->initializer2fb069a && ($this->initializer2fb069a->__invoke($valueHolder2fb069a, $this, 'onPublish', array('conn' => $conn, 'topic' => $topic, 'request' => $request, 'event' => $event, 'exclude' => $exclude, 'eligible' => $eligible), $this->initializer2fb069a) || 1) && $this->valueHolder2fb069a = $valueHolder2fb069a;
        return $this->valueHolder2fb069a->onPublish($conn, $topic, $request, $event, $exclude, $eligible);
    }
    public function dispatch($calledMethod, \Ratchet\ConnectionInterface $conn, \Ratchet\Wamp\Topic $topic, \Gos\Bundle\WebSocketBundle\Router\WampRequest $request, $payload = null, $exclude = null, $eligible = null, $provider = null)
    {
        $this->initializer2fb069a && ($this->initializer2fb069a->__invoke($valueHolder2fb069a, $this, 'dispatch', array('calledMethod' => $calledMethod, 'conn' => $conn, 'topic' => $topic, 'request' => $request, 'payload' => $payload, 'exclude' => $exclude, 'eligible' => $eligible, 'provider' => $provider), $this->initializer2fb069a) || 1) && $this->valueHolder2fb069a = $valueHolder2fb069a;
        return $this->valueHolder2fb069a->dispatch($calledMethod, $conn, $topic, $request, $payload, $exclude, $eligible, $provider);
    }
    public function __construct($initializer)
    {
        $this->initializer2fb069a = $initializer;
    }
    public function & __get($name)
    {
        $this->initializer2fb069a && ($this->initializer2fb069a->__invoke($valueHolder2fb069a, $this, '__get', array('name' => $name), $this->initializer2fb069a) || 1) && $this->valueHolder2fb069a = $valueHolder2fb069a;
        if (isset(self::$publicProperties2fb069a[$name])) {
            return $this->valueHolder2fb069a->$name;
        }
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2fb069a;
            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }
        $targetObject = $this->valueHolder2fb069a;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer2fb069a && ($this->initializer2fb069a->__invoke($valueHolder2fb069a, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer2fb069a) || 1) && $this->valueHolder2fb069a = $valueHolder2fb069a;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2fb069a;
            return $targetObject->$name = $value;;
            return;
        }
        $targetObject = $this->valueHolder2fb069a;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer2fb069a && ($this->initializer2fb069a->__invoke($valueHolder2fb069a, $this, '__isset', array('name' => $name), $this->initializer2fb069a) || 1) && $this->valueHolder2fb069a = $valueHolder2fb069a;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2fb069a;
            return isset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolder2fb069a;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer2fb069a && ($this->initializer2fb069a->__invoke($valueHolder2fb069a, $this, '__unset', array('name' => $name), $this->initializer2fb069a) || 1) && $this->valueHolder2fb069a = $valueHolder2fb069a;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2fb069a;
            unset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolder2fb069a;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __clone()
    {
        $this->initializer2fb069a && ($this->initializer2fb069a->__invoke($valueHolder2fb069a, $this, '__clone', array(), $this->initializer2fb069a) || 1) && $this->valueHolder2fb069a = $valueHolder2fb069a;
        $this->valueHolder2fb069a = clone $this->valueHolder2fb069a;
    }
    public function __sleep()
    {
        $this->initializer2fb069a && ($this->initializer2fb069a->__invoke($valueHolder2fb069a, $this, '__sleep', array(), $this->initializer2fb069a) || 1) && $this->valueHolder2fb069a = $valueHolder2fb069a;
        return array('valueHolder2fb069a');
    }
    public function __wakeup()
    {
    }
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer2fb069a = $initializer;
    }
    public function getProxyInitializer()
    {
        return $this->initializer2fb069a;
    }
    public function initializeProxy()
    {
        return $this->initializer2fb069a && ($this->initializer2fb069a->__invoke($valueHolder2fb069a, $this, 'initializeProxy', array(), $this->initializer2fb069a) || 1) && $this->valueHolder2fb069a = $valueHolder2fb069a;
    }
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder2fb069a;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder2fb069a;
    }
}
