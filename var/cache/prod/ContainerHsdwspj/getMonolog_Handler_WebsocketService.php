<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'monolog.handler.websocket' shared service.

$this->services['monolog.handler.websocket'] = $instance = new \Symfony\Bridge\Monolog\Handler\ConsoleHandler(NULL, true, array(32 => 200, 16 => 400, 64 => 250, 128 => 200, 256 => 100));

$instance->pushProcessor(${($_ = isset($this->services['monolog.processor.psr_log_message']) ? $this->services['monolog.processor.psr_log_message'] : $this->services['monolog.processor.psr_log_message'] = new \Monolog\Processor\PsrLogMessageProcessor()) && false ?: '_'});

return $instance;
