<?php

class ClientEventListener_d2113e1 extends \Gos\Bundle\WebSocketBundle\Event\ClientEventListener implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolderd2113e1 = null;
    private $initializerd2113e1 = null;
    private static $publicPropertiesd2113e1 = array(
        
    );
    public function onClientConnect(\Gos\Bundle\WebSocketBundle\Event\ClientEvent $event)
    {
        $this->initializerd2113e1 && ($this->initializerd2113e1->__invoke($valueHolderd2113e1, $this, 'onClientConnect', array('event' => $event), $this->initializerd2113e1) || 1) && $this->valueHolderd2113e1 = $valueHolderd2113e1;
        return $this->valueHolderd2113e1->onClientConnect($event);
    }
    public function onClientDisconnect(\Gos\Bundle\WebSocketBundle\Event\ClientEvent $event)
    {
        $this->initializerd2113e1 && ($this->initializerd2113e1->__invoke($valueHolderd2113e1, $this, 'onClientDisconnect', array('event' => $event), $this->initializerd2113e1) || 1) && $this->valueHolderd2113e1 = $valueHolderd2113e1;
        return $this->valueHolderd2113e1->onClientDisconnect($event);
    }
    public function onClientError(\Gos\Bundle\WebSocketBundle\Event\ClientErrorEvent $event)
    {
        $this->initializerd2113e1 && ($this->initializerd2113e1->__invoke($valueHolderd2113e1, $this, 'onClientError', array('event' => $event), $this->initializerd2113e1) || 1) && $this->valueHolderd2113e1 = $valueHolderd2113e1;
        return $this->valueHolderd2113e1->onClientError($event);
    }
    public function onClientRejected(\Gos\Bundle\WebSocketBundle\Event\ClientRejectedEvent $event)
    {
        $this->initializerd2113e1 && ($this->initializerd2113e1->__invoke($valueHolderd2113e1, $this, 'onClientRejected', array('event' => $event), $this->initializerd2113e1) || 1) && $this->valueHolderd2113e1 = $valueHolderd2113e1;
        return $this->valueHolderd2113e1->onClientRejected($event);
    }
    public function __construct($initializer)
    {
        $this->initializerd2113e1 = $initializer;
    }
    public function & __get($name)
    {
        $this->initializerd2113e1 && ($this->initializerd2113e1->__invoke($valueHolderd2113e1, $this, '__get', array('name' => $name), $this->initializerd2113e1) || 1) && $this->valueHolderd2113e1 = $valueHolderd2113e1;
        if (isset(self::$publicPropertiesd2113e1[$name])) {
            return $this->valueHolderd2113e1->$name;
        }
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd2113e1;
            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }
        $targetObject = $this->valueHolderd2113e1;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializerd2113e1 && ($this->initializerd2113e1->__invoke($valueHolderd2113e1, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerd2113e1) || 1) && $this->valueHolderd2113e1 = $valueHolderd2113e1;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd2113e1;
            return $targetObject->$name = $value;;
            return;
        }
        $targetObject = $this->valueHolderd2113e1;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializerd2113e1 && ($this->initializerd2113e1->__invoke($valueHolderd2113e1, $this, '__isset', array('name' => $name), $this->initializerd2113e1) || 1) && $this->valueHolderd2113e1 = $valueHolderd2113e1;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd2113e1;
            return isset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolderd2113e1;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializerd2113e1 && ($this->initializerd2113e1->__invoke($valueHolderd2113e1, $this, '__unset', array('name' => $name), $this->initializerd2113e1) || 1) && $this->valueHolderd2113e1 = $valueHolderd2113e1;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderd2113e1;
            unset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolderd2113e1;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __clone()
    {
        $this->initializerd2113e1 && ($this->initializerd2113e1->__invoke($valueHolderd2113e1, $this, '__clone', array(), $this->initializerd2113e1) || 1) && $this->valueHolderd2113e1 = $valueHolderd2113e1;
        $this->valueHolderd2113e1 = clone $this->valueHolderd2113e1;
    }
    public function __sleep()
    {
        $this->initializerd2113e1 && ($this->initializerd2113e1->__invoke($valueHolderd2113e1, $this, '__sleep', array(), $this->initializerd2113e1) || 1) && $this->valueHolderd2113e1 = $valueHolderd2113e1;
        return array('valueHolderd2113e1');
    }
    public function __wakeup()
    {
    }
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializerd2113e1 = $initializer;
    }
    public function getProxyInitializer()
    {
        return $this->initializerd2113e1;
    }
    public function initializeProxy()
    {
        return $this->initializerd2113e1 && ($this->initializerd2113e1->__invoke($valueHolderd2113e1, $this, 'initializeProxy', array(), $this->initializerd2113e1) || 1) && $this->valueHolderd2113e1 = $valueHolderd2113e1;
    }
    public function isProxyInitialized()
    {
        return null !== $this->valueHolderd2113e1;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderd2113e1;
    }
}
