<?php

class PeriodicMemoryUsage_e8f4ac8 extends \Gos\Bundle\WebSocketBundle\Periodic\PeriodicMemoryUsage implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHoldere8f4ac8 = null;
    private $initializere8f4ac8 = null;
    private static $publicPropertiese8f4ac8 = array(
        
    );
    public function tick()
    {
        $this->initializere8f4ac8 && ($this->initializere8f4ac8->__invoke($valueHoldere8f4ac8, $this, 'tick', array(), $this->initializere8f4ac8) || 1) && $this->valueHoldere8f4ac8 = $valueHoldere8f4ac8;
        return $this->valueHoldere8f4ac8->tick();
    }
    public function getTimeout()
    {
        $this->initializere8f4ac8 && ($this->initializere8f4ac8->__invoke($valueHoldere8f4ac8, $this, 'getTimeout', array(), $this->initializere8f4ac8) || 1) && $this->valueHoldere8f4ac8 = $valueHoldere8f4ac8;
        return $this->valueHoldere8f4ac8->getTimeout();
    }
    public function __construct($initializer)
    {
        $this->initializere8f4ac8 = $initializer;
    }
    public function & __get($name)
    {
        $this->initializere8f4ac8 && ($this->initializere8f4ac8->__invoke($valueHoldere8f4ac8, $this, '__get', array('name' => $name), $this->initializere8f4ac8) || 1) && $this->valueHoldere8f4ac8 = $valueHoldere8f4ac8;
        if (isset(self::$publicPropertiese8f4ac8[$name])) {
            return $this->valueHoldere8f4ac8->$name;
        }
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldere8f4ac8;
            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }
        $targetObject = $this->valueHoldere8f4ac8;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializere8f4ac8 && ($this->initializere8f4ac8->__invoke($valueHoldere8f4ac8, $this, '__set', array('name' => $name, 'value' => $value), $this->initializere8f4ac8) || 1) && $this->valueHoldere8f4ac8 = $valueHoldere8f4ac8;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldere8f4ac8;
            return $targetObject->$name = $value;;
            return;
        }
        $targetObject = $this->valueHoldere8f4ac8;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializere8f4ac8 && ($this->initializere8f4ac8->__invoke($valueHoldere8f4ac8, $this, '__isset', array('name' => $name), $this->initializere8f4ac8) || 1) && $this->valueHoldere8f4ac8 = $valueHoldere8f4ac8;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldere8f4ac8;
            return isset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHoldere8f4ac8;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializere8f4ac8 && ($this->initializere8f4ac8->__invoke($valueHoldere8f4ac8, $this, '__unset', array('name' => $name), $this->initializere8f4ac8) || 1) && $this->valueHoldere8f4ac8 = $valueHoldere8f4ac8;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldere8f4ac8;
            unset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHoldere8f4ac8;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __clone()
    {
        $this->initializere8f4ac8 && ($this->initializere8f4ac8->__invoke($valueHoldere8f4ac8, $this, '__clone', array(), $this->initializere8f4ac8) || 1) && $this->valueHoldere8f4ac8 = $valueHoldere8f4ac8;
        $this->valueHoldere8f4ac8 = clone $this->valueHoldere8f4ac8;
    }
    public function __sleep()
    {
        $this->initializere8f4ac8 && ($this->initializere8f4ac8->__invoke($valueHoldere8f4ac8, $this, '__sleep', array(), $this->initializere8f4ac8) || 1) && $this->valueHoldere8f4ac8 = $valueHoldere8f4ac8;
        return array('valueHoldere8f4ac8');
    }
    public function __wakeup()
    {
    }
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializere8f4ac8 = $initializer;
    }
    public function getProxyInitializer()
    {
        return $this->initializere8f4ac8;
    }
    public function initializeProxy()
    {
        return $this->initializere8f4ac8 && ($this->initializere8f4ac8->__invoke($valueHoldere8f4ac8, $this, 'initializeProxy', array(), $this->initializere8f4ac8) || 1) && $this->valueHoldere8f4ac8 = $valueHoldere8f4ac8;
    }
    public function isProxyInitialized()
    {
        return null !== $this->valueHoldere8f4ac8;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHoldere8f4ac8;
    }
}
