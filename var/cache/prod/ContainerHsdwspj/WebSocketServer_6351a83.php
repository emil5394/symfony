<?php

class WebSocketServer_6351a83 extends \Gos\Bundle\WebSocketBundle\Server\Type\WebSocketServer implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6351a83 = null;
    private $initializer6351a83 = null;
    private static $publicProperties6351a83 = array(
        
    );
    public function setSessionHandler(\SessionHandlerInterface $sessionHandler)
    {
        $this->initializer6351a83 && ($this->initializer6351a83->__invoke($valueHolder6351a83, $this, 'setSessionHandler', array('sessionHandler' => $sessionHandler), $this->initializer6351a83) || 1) && $this->valueHolder6351a83 = $valueHolder6351a83;
        return $this->valueHolder6351a83->setSessionHandler($sessionHandler);
    }
    public function launch($host, $port, $profile)
    {
        $this->initializer6351a83 && ($this->initializer6351a83->__invoke($valueHolder6351a83, $this, 'launch', array('host' => $host, 'port' => $port, 'profile' => $profile), $this->initializer6351a83) || 1) && $this->valueHolder6351a83 = $valueHolder6351a83;
        return $this->valueHolder6351a83->launch($host, $port, $profile);
    }
    public function getName()
    {
        $this->initializer6351a83 && ($this->initializer6351a83->__invoke($valueHolder6351a83, $this, 'getName', array(), $this->initializer6351a83) || 1) && $this->valueHolder6351a83 = $valueHolder6351a83;
        return $this->valueHolder6351a83->getName();
    }
    public function __construct($initializer)
    {
        $this->initializer6351a83 = $initializer;
    }
    public function & __get($name)
    {
        $this->initializer6351a83 && ($this->initializer6351a83->__invoke($valueHolder6351a83, $this, '__get', array('name' => $name), $this->initializer6351a83) || 1) && $this->valueHolder6351a83 = $valueHolder6351a83;
        if (isset(self::$publicProperties6351a83[$name])) {
            return $this->valueHolder6351a83->$name;
        }
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6351a83;
            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;;
            return;
        }
        $targetObject = $this->valueHolder6351a83;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer6351a83 && ($this->initializer6351a83->__invoke($valueHolder6351a83, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer6351a83) || 1) && $this->valueHolder6351a83 = $valueHolder6351a83;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6351a83;
            return $targetObject->$name = $value;;
            return;
        }
        $targetObject = $this->valueHolder6351a83;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer6351a83 && ($this->initializer6351a83->__invoke($valueHolder6351a83, $this, '__isset', array('name' => $name), $this->initializer6351a83) || 1) && $this->valueHolder6351a83 = $valueHolder6351a83;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6351a83;
            return isset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolder6351a83;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer6351a83 && ($this->initializer6351a83->__invoke($valueHolder6351a83, $this, '__unset', array('name' => $name), $this->initializer6351a83) || 1) && $this->valueHolder6351a83 = $valueHolder6351a83;
        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6351a83;
            unset($targetObject->$name);;
            return;
        }
        $targetObject = $this->valueHolder6351a83;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \stdClass();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __clone()
    {
        $this->initializer6351a83 && ($this->initializer6351a83->__invoke($valueHolder6351a83, $this, '__clone', array(), $this->initializer6351a83) || 1) && $this->valueHolder6351a83 = $valueHolder6351a83;
        $this->valueHolder6351a83 = clone $this->valueHolder6351a83;
    }
    public function __sleep()
    {
        $this->initializer6351a83 && ($this->initializer6351a83->__invoke($valueHolder6351a83, $this, '__sleep', array(), $this->initializer6351a83) || 1) && $this->valueHolder6351a83 = $valueHolder6351a83;
        return array('valueHolder6351a83');
    }
    public function __wakeup()
    {
    }
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer6351a83 = $initializer;
    }
    public function getProxyInitializer()
    {
        return $this->initializer6351a83;
    }
    public function initializeProxy()
    {
        return $this->initializer6351a83 && ($this->initializer6351a83->__invoke($valueHolder6351a83, $this, 'initializeProxy', array(), $this->initializer6351a83) || 1) && $this->valueHolder6351a83 = $valueHolder6351a83;
    }
    public function isProxyInitialized()
    {
        return null !== $this->valueHolder6351a83;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6351a83;
    }
}
